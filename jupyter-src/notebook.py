#%% Import the things
import pyaudio
import wave
import numpy as np
import scipy.io as scio # To resolve some wierd import issues that were encountered

#%% Just use this cell to figure out which device to use. 
p = pyaudio.PyAudio()
p.get_device_info_by_index(5)

#%%  RECORDING SETTINGS
CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 48000 # RATE for the UGC102
RECORD_SECONDS = 10
WAVE_OUTPUT_FILENAME = "output.wav"

#%%  RECORD THE AUDIO
# p = pyaudio.PyAudio()
stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                output=True,
                input_device_index=1,
                output_device_index=5,
                frames_per_buffer=CHUNK
                )

print("* recording")
frames = []
for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    data = stream.read(CHUNK)
    stream.write(data, CHUNK)
    frames.append(data)
print("* done recording")
stream.stop_stream()
stream.close()
p.terminate()

#%% WRITE AUDIO TO WAV FILE
wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
wf.setnchannels(CHANNELS)
wf.setsampwidth(p.get_sample_size(FORMAT))
wf.setframerate(RATE)
wf.writeframes(b''.join(frames))
wf.close()